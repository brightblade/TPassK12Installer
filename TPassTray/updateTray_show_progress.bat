@ECHO OFF
ECHO Hello. We're going to update the Eyemetric tray application.
ECHO First we'll remove the existing version....just one moment...
.\setup.exe /v"/qn REMOVE=ALL"
ECHO Great! Now we'll install the updated version....one moment
.\setup.exe /v" USERNAME=%username% /L*V tray.log"
ECHO Installing certificate
certutil.exe -p "" -importpfx "C:\\Program Files (x86)\\Eyemetric\\EyemetricTray\\Eyemetric.p12"
ECHO adding urlacl for https
netsh http add urlacl url=https://+:8390/ user=\Everyone
Url reservation add failed, Error: 183
ECHO All finished! Have a great day
PAUSE
