@ECHO OFF
ECHO Hello. Let's update the Eyemetric tray application.
ECHO We'll remove the existing version....one moment..
.\setup.exe /S /v"/qn REMOVE=ALL"
:: ECHO Great. Now we'll install the updated version....almost there...
.\setup.exe /S /v"/qn"
ECHO All finished. Have a great day!
PAUSE