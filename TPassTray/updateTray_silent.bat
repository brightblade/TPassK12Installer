@ECHO OFF
ECHO Hello. Let's update the Eyemetric tray application.
:: ECHO We'll remove the existing version....
.\setup.exe /S /v"/qn REMOVE=ALL"
:: ECHO Great. Now we'll install the updated version....
.\setup.exe /S /v"/qn"
:: ECHO All finished. Have a great day!
